import {EMPTY, fromEvent} from 'rxjs'
import {map, debounceTime, distinctUntilChanged, switchMap, mergeMap, tap, catchError, filter} from 'rxjs/operators'
import {ajax} from 'rxjs/ajax'

const url = 'https://api.github.com/search/users?q='

const search = document.getElementById('search')
const result = document.getElementById('result')

const createUserCard = (user) => {
  return `
    <div class="card">
      <div class="card-image">
        <img src="${user.avatar_url}" />
        <span class="card-title">${user.login}</span>
      </div>
      <div class="card-action">
        <a href="${user.html_url}" target="_blank">Открыть github</a>
      </div>
    </div>
  `
}

const stream$ = fromEvent(search, 'input')
  .pipe(
    map(event => event.target.value),
    debounceTime(1000), // ждем когда пользователь прекратит ввод
    distinctUntilChanged(), // пропускает поток если поменялось значение
    tap(() => result.innerHTML = ''),
    filter(data => data.trim()),
    switchMap(data => ajax.getJSON(`${url}${data}`) // переключаемся на новый поток
      .pipe(
        catchError(error => EMPTY) // отлавливаем ошибку, возвращаем пустой Observable
      )
    ),
    map(response => response.items), // извлекаем только массив пользователей
    mergeMap(items => items) // для каждого пользователя дергаем подписку
  )


stream$.subscribe(user => {
  const html = createUserCard(user)
  result.insertAdjacentHTML('beforeend', html)
})


